//materi 21

const express = require('express');
const app = express();

const courses = [
	{ id: 1, name: 'course1'},
	{ id: 2, name: 'course2'},
	{ id: 3, name: 'course3'},
];


// create index page
app.get('/', (req, res) => {
	res.send('Hello NODE JS!, MERDEKAA!!');
});

/*
// new route
app.get('/api/courses', (req, res) => {
	res.send(courses);
});
*/  
//res.send('id: ' + req.query.id);
app.get('/api/courses/:id', (req, res) => {
 const course = courses.find(c => c.id === parseInt(req.params.id));
	 if (!course) res.status(404).send('Course tidak ditemukan');
	 res.send(course);
});

//materi 22
// http post request
app.post('/api/courses', (req, res) => {
	
	// membuat course object
	const course = {
		id: courses.length + 1,
		name: req.body.name
	};
	courses.push(course);
	
	/* menampilkan course baru */
	res.send(course);
	
});



app.listen(3000, () => console.log("Listeining on port 3000..."));

 